<?php
/**
 * @file
 * farm_app_variety.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function farm_app_variety_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create variety content'.
  $permissions['create variety content'] = array(
    'name' => 'create variety content',
    'roles' => array(
      'administrator' => 'administrator',
      'farmer' => 'farmer',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any variety content'.
  $permissions['delete any variety content'] = array(
    'name' => 'delete any variety content',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own variety content'.
  $permissions['delete own variety content'] = array(
    'name' => 'delete own variety content',
    'roles' => array(
      'administrator' => 'administrator',
      'farmer' => 'farmer',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any variety content'.
  $permissions['edit any variety content'] = array(
    'name' => 'edit any variety content',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own variety content'.
  $permissions['edit own variety content'] = array(
    'name' => 'edit own variety content',
    'roles' => array(
      'administrator' => 'administrator',
      'farmer' => 'farmer',
    ),
    'module' => 'node',
  );

  return $permissions;
}
