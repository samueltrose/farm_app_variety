<?php
/**
 * @file
 * farm_app_variety.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function farm_app_variety_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function farm_app_variety_node_info() {
  $items = array(
    'variety' => array(
      'name' => t('Variety'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Name'),
      'help' => '',
    ),
  );
  return $items;
}
